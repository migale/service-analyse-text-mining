# Service d'analyse en text mining sur Migale
 
La littérature scientifique représente une source riche d'information précieuse pour la recherche. Les acteurs de la recherche exploitent cette information, à travers notamment la veille, pour suivre et accéder à des informations qui alimentent, justifient et soutiennent les expériences. Cependant compte tenu du contexte de masse de données grandissantes et hétérogènes, il est difficile d'accéder de manière large aux connaissances fines exprimées dans les publications scientifiques en utilisant les outils traditionnels. Migale propose ainsi un levier basé sur le text mining pour faciliter l'exploitation des données de la littérature en sciences de la vie. Il s'agit d'une offre de service d'analyse en text mining dont l'idée est d'aider les acteurs de la recherche en science de la vie à analyser et à extraire des informations fines contenues dans des textes bruts (d'articles scientifiques, de champs de base de données,…) en s'appuyant sur son expertise dans les services d'analyse en bioinformatique et sur les technologies développées dans les domaines de l'extraction des connaissances.
 
## Offre de service
 
Elle vise à collaborer et accompagner sur demande ses utilisateurs sur des mini-projets d'extraction d'informations à partir de textes bruts issus de la littérature scientifique ou de bases de données en bioinformatique
 
## Scope du service d'analyse
 
Les projets d'analyse doivent correspondre à des applications du text mining pour les sciences de la vie; Les types d'analyse visés sont les suivants :
 
## 1. Constitution de corpus textuels thématiques
 
Identification, collecte et confection de corpus (ensemble de documents textuels) qui parle/traitent d'un sujet donné à partir de sources disponibles sur le Web.
 
* Thématiques couvertes : toutes les thématiques en sciences de la vie
* Sources couvertes : Bases de données telles que PubMed, Istex et/ou autres sources en fonction de l'accessibilité (légale, technique) aux documents et de la présence des informations à extraire dans les textes.

    **Exemple :** collecter un ensemble d'articles scientifiques qui parlent de bactéries et d'habitats de bactéries à partir de la banque PubMed. Cela consiste à (1) identifier les termes relatifs au domaine des microorganismes et habitats, (2) utiliser ensuite les termes identifiés dans une requête pour retrouver les articles dans la base PubMed et (3) fournir les resultats (livrable) sous forme d'un ensemble de textes traitant de bactéries et habitats; on appelle cette ensemble *corpus*.

    Cet exemple montre des résumés PubMed selectionnés qui traitent des bactéries et d'habitats de bactéries

![corpus](images/corpus.png)
   
## 2. Extraction d'entités nommées
 
Extraction de mentions d'entités biologiques (ex: mentions de noms de bactéries, d'habitats, de phénotypes, de métabolites, etc.) dans des corpus de textes prédéfinis ou à définir.

* Thématiques : thématiques en microbiologie
* Type d'information à extraire : entités biologiques explicitement mentionnées dans les textes bruts selon l’existence de lexiques, dictionnaires, référentiels, règles d'écriture, données annotées sur les entités à extraire.

    **Exemple :** retrouver les bactéries et les d'habitats de bactéries mentionnés dans un ensemble d'articles scientifiques. Cela consiste à (1) repérer dans les textes les expressions qui correspondent à des bactéries et à des habitats de bactéries et (2) fournir le résultat dans un fichier sous forme d'une liste de bactéries et habitats avec leurs localisations dans les textes.

    Cet exemple montre un résumé PubMed où on retrouve des mentions de bactéries et d'habitats

   ![annotation](images/annotation.png)
 
 
## 3. Classification d’informations textuelles
 
Catégorisation d'informations textuelles pouvant être des documents ou des éléments composant un document (phrases, entités nommées, annotations, etc.).

* Thématiques : toutes les thématiques en sciences de la vie selon l’existence de données d’apprentissage
* Types d'information à classer : documents, phrases, entités biologiques, annotations, etc.

    **Exemple :** classifier les phases de textes exprimant la localisation de bacteries dans des habitats. Cela consiste à (1) constituer avec des experts du domaine un ensemble d'appentissage (textes annotés où on classe les phrases contenant une mention de localisation d'un bactérie dans un habitat) permettant aux algorithmes d'appendre à classer les phrases qui expriment qu'une bacterie est localisée dans un habitat, (2) utiliser ensuite cet ensemble d'apprentissage pour produire un modèle de classification de phrases contenant des liens de localisaton et (3) fournir le modéle de classification comme résultat pour la détection des phrases contenant des localisations à partir de nouveaux textes.  

    Cet exemple montre un localisation trouvé dans une phrase d'un résumé PubMed

    ![localisation](images/localisation.png)
 
<! Le framework AlvisNLP sera utilisé pour effectuer les analyses text mining. Des services en ligne seront proposés pour aider dans l'exploitation des résultats et la constitution des ressources de text mining : AlvisIR (moteur de recherche sémantique), Tydi (contruction de terminologies), AlvisAE (annotation de documents textuels).>

---
**NOTE**

[Voici un exemple de projet d'analyse](https://forgemia.inra.fr/migale/extraction-genes-metabolites) qui vise à extraire des liens fonctionnels entre gènes et métabolites avec les tâches suivantes (**exemple à ne pas publier en attendant l'accord des partenaires**):
 
* confection du corpus thématique qui rassemble des résumés traitant de pathways à partir de PubMed
* extraction de cooccurrences de gènes-métabolites dans des phrases des résumés PubMed
* classification des phrases contenant des liens gènes-métabolites
---

## Prérequis
 
Les demandeurs doivent être en mesure d'exprimer leurs besoins selon une combinaison des applications et thématiques précisées plus haut. Ils doivent disposer des ressources pour prendre en charge certaines tâches techniques (ex: préparation des lexiques à utiliser pour l'extraction d'information, formalisation des requêtes à utiliser pour la confection de corpus, exploitation des résultats issus de l'extraction, etc.). Il est également indispensable d'impliquer des experts du domaine qui puissent aider à caractériser les informations à traiter et à interpréter les résultats des analyses.
 
Les informations à extraire doivent être exprimées dans les textes pleins et disponibles dans des sources accessibles; les textes présents dans des images, tableaux, schémas ou formules ne sont pas concernés.
 
## Soumission des demandes
 
** Les demandes doivent être faites via un formulaire sur le site web de Migale**

Les demandeurs devront décrire leur(s) besoin(s) acccompagnés des informations permettant d'évaluer la faisabilité :
* motivation/besoin
* objet de l'étude
* thématiques de l'étude, entités biologiques à extraire, information à classer, etc.
* participants
* disponibilité des données d'entrée
* utilisation prévue des résultats
* etc.
 
## Traitement des demandes
 
Le traitement des demandes va suivre un processus d'évaluation qui tient compte de critères permettant d'assurer une réponse adaptée (besoins, informations à extraire, faisabilité,  qualité des résultats attendus, solutions disponibles, durée, cadre légal, etc.)
 
La conception/mise en oeuvre d'une demande acceptée est effectuée dans le cadre d'un projet collaboratif entre les demandeurs et Migale. Migale se chargera de mobiliser les outils et les ressources de text mining  permettant de répondre aux besoins. Les demandeurs se chargeront d'apporter l'expertise dans la thématique d'étude (fournir les informations et les données nécessaires pour alimenter les outils de text mining) et exploiter les résultats d'analyse.

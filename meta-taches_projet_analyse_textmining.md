 
# Traitement des demandes d'analyse de text mining
 
La réponse à une demande d'analyse s'effectue dans le cadre d'un projet collaboratif entre les demandeurs et Migale sur une période déterminée (plusieurs mois). Les objectifs du projet et les modalités d'exécution sont anticipés autant que possible par les parties prenantes avant le début d'exécution.
 
Pour schématiser, un projet d'analyse de text mining dans le cadre de l'offre de service va comporter les étapes suivantes :
 
## Préparation/Soumission de la demande
 
Les demandeurs préparent leur besoin et les objectifs visés qu'ils soumettent à Migale
 
## Etude de la demande
 
Migale étudie la demande et formule sa réponse. Des interactions peuvent avoir lieu à ce niveau pour préciser ou redéfinir certains points de la demande.
 
## Définition du cahier des charges
 
Si la demande est acceptée, les parties prenantes précisent ensemble les besoins, les ressources et les résultats attendus. Ils définissent le calendrier, les rôles des participants, les entrées et les livrables attendus.
 
## Mise en oeuvre du text mining
 
Les tâches de text mining pilotées par Migale sont exécutées selon les termes discutés. Des tâches annexes pilotées par les demandeurs sont ajoutées selon les besoins pour l'accès aux ressources en entrée, l'analyse et l'exploitation des résultats de text mining.
 
Voici quelques tâches types qu'on retrouve dans la mise en oeuvre
 
**Définition des Infos et Sources**
 
Cette tâche consiste à définir les informations à traiter ainsi qu'à éventuellement sélectionner les sources à utiliser pour accéder aux textes.
* Il peut être nécessaire de formaliser les informations à analyser ou le problème à traiter (formaliser les requêtes pour la confection de corpus, définir la types d'entités à extraires ou poser le problème de classification à aborder)
* Concernant les sources, il en existe plusieyrs (Pubmed, Elsevier, Springer, Istex, WoS, etc.); on tient en général compte de plusieurs critères (accessibilité, pérennité, qualité, couverture, etc.) pour sélectionner les sources à utiliser
 
**Traitement text mining**
 
effectuer le ou les analyses de text mining selon les définitions lors de la tâche précédente. Les analysent peuvent être une combinaison des tâches suivantes :
 
* **Confectionner un corpus** : sélectionner un ensemble d'articles pertinents et construire des corpus thématiques. Les demandeurs apportent si nécessaire les entrées (par exemple, la liste de mots clés du domaine ou les termes MESH) pour élaborer les requêtes permettant d'identifier les articles pertinents en fonction des sources. Les demandeurs se chargent également de suivre et de valider, au moins partiellement, les articles sélectionnés.
Les livrables de cette tâche peuvent être un corpus thématique ou/et la procédure (automatique) de construction du corpus
 
* **Extraction des informations** Extraire les informations formalisées à partir d'un corpus. Le corpus peut être le résultat d'une tâche de confection de corpus ou il peut être fourni comme données d'entrée. L'extraction peut nécessiter de revenir à la de confection de corpus pour élargir ou améliorer la qualité des résultats.
Les livrables de cette tâche peuvent les résulats d'un pipeline d'extraction sur un corpus ou un pipeline d'extraction
 
* **Classification des informations** : classifier les informations formalisées à partir d'un corpus. Le corpus peut être le résultat d'une tâche de confection de corpus ou il peut être élaboré et fourni comme données d'entrée. 
Les livrables de cette tâche peuvent être des résultats d'un pipeline de classification sur des données ou le pipeline de classification.
 
**Analyse et exploitation des résultats** : il s'agit d'analyser, évaluer ou/et exploiter des résultats des analyses selon une forme qui sera à définir.
 
<!--
 
ajouter une étape **Valorisation** ?
nous aurons besoin d'un outil d'explotation de corpus qui serait destiné aux utilisateurs
 
Les outils prévus pour
* outils de traitement
 * AlvisNLP
* outils d'aide
 * exploration des annotations : AlvisIR
 * exploration des corpus : Lodex, Gephi,
Outils de gestion de projet
Outils de gestion de rapport
 

 
 
```mermaid
graph TB
 subgraph "Cycle d'un projet d'analyse"
 
 Node41[Définition des informations à extraire et selection des sources] -- Node42[Constitution de Corpus]
 Node42[Constitution de Corpus] -- Node43[Extraction des informations]
 Node42[Constitution de Corpus] -- Node44[Analyse des résulats]
 Node43[Extraction des informations] -- Node44[Analyse des résulats]
 
 end
```
 
 -->

